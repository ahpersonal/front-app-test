// Logik bitte hier
document.addEventListener('DOMContentLoaded', function() {
    // var slideIndex = 0;
    showSlides(0);
}, false);

/*document.onreadystatechange = function() {
    if (document.readyState === 'complete') {
        var slideIndex = 0;
        showSlides();
    }
}*/
/*window.onload = function() {
    var slideIndex = 0;
    showSlides();
}*/

function showSlides(index) {
    var slideIndex = index;
    var i;
    var slides = document.getElementsByClassName("part");
    var dots = document.getElementsByClassName("dot");
    var imgs = document.getElementsByClassName("partimg");
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex > slides.length) {
        slideIndex = 1
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" activedot", "");
    }
    slides[slideIndex - 1].style.display = "block";
    var img = imgs[slideIndex - 1];
    if (img.getAttribute('temp-src')) {
        img.setAttribute('src', img.getAttribute('temp-src'));
        img.onload = function() {
            img.removeAttribute('temp-src');
        };
    }

    dots[slideIndex - 1].className += " activedot";
    setTimeout('showSlides(' + slideIndex + ')', 2000); // Change image every 2 seconds
}